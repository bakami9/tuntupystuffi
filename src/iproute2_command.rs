use std::process::Command;
use tun_tap::{Iface, Mode};
use anyhow::Result;

//TODO impl as trait for abstaction
//of tun/tap device

pub fn mktap<T: Into<String>>(name: T) -> Result<Iface> {
    let iface_tun = Iface::new(&name.into(), Mode::Tun)?;


    //TODO add proper error handling with anyhow's .context()
    Command::new("ip")
        .args(&["addr", "add", "dev", iface_tun.name(),
        "192.168.100.1/24"])
        .spawn()?
        .wait()?;


    Ok(iface_tun)
}

pub fn set_interface_up(iface: &Iface) -> Result<()> {
    Command::new("ip")
        .args(&["link", "set", "dev", iface.name(),
        "up"])
        .spawn()?
        .wait()?;

    Ok(())
}


pub fn set_interface_down(iface: &Iface) -> Result<()> {
    Command::new("ip")
        .args(&["link", "set", "dev", iface.name(),
        "down"])
        .spawn()?
        .wait()?;

    Ok(())
}
