//maybe use an netlink library or sth for that
use anyhow::Result;

use packet::ether;
use packet::ip::v4;
use packet::ip;
use packet::icmp;

mod iproute2_command;

use iproute2_command::{
    mktap,
    set_interface_up,
    set_interface_down,
};

use packet::AsPacket;
use packet::Packet;


fn handle_ipv4<T: AsRef<[u8]> + Packet> (packet: v4::Packet<T>) -> Result<()> {
    match packet.protocol() {
        ip::Protocol::Icmp => {
            //packet.into();
        },
        _ => println!("unknown protocol \"{:?}\" encapsulated in ipv4 header",
                      packet.protocol()),
    }
    Ok(())
}

fn main() -> Result<()> {

    let iface_tun = mktap("initaltap0")?;
    set_interface_up(&iface_tun)?;
    

    let mut buf = vec![0; 1504];

    loop {
        iface_tun.recv(&mut buf)?;
       

        //println!("magic proto shit {:x?}", buf.remove(0));
        //println!("magic proto shit {:x?}", buf.remove(0));
        buf.remove(0);
        buf.remove(0);


        let ethertype = ((buf.remove(0) as u16) << 8) + buf.remove(0) as u16;

        match ethertype.into() {
            ether::Protocol::Ipv4 => handle_ipv4(buf.as_packet()?)?,
            _ => println!("unknown protocol \"{:?}\"", ether::Protocol::from(ethertype)),
        }


        //let packet: packet::ip::v4::Packet<_> =  buf.as_packet()?;
        //println!("{:?}",packet.protocol());
        
        print!("\n");
    }
    
    set_interface_down(&iface_tun)?;
    

}
